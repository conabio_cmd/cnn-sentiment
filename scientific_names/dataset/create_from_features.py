'''
Created on Jan 28, 2016

Script to create a train and test lmdb file using a positive and
negative numpy.ndarray.

@author: Daniel
'''
import os
import pickle
import numpy as np
from sklearn.cross_validation import train_test_split
import lmdb
import caffe
import threading
import contextlib

lmdb_global_write_lock = threading.Lock()


@contextlib.contextmanager
def lmdb_write_txn(env):
    with lmdb_global_write_lock:
        with env.begin(write=True) as txn:
            yield txn


if __name__ == '__main__':
    # Path where the positive.pkl and negative.pkl are stored.
    pkl_dir = '/Users/Daniel/Documents/workspace/cnn-sentiment/words_model'
    with open(os.path.join(pkl_dir, 'positive.pkl'), 'rb') as f:
        positive = pickle.load(f)
    with open(os.path.join(pkl_dir, 'negative.pkl'), 'rb') as f:
        negative = pickle.load(f)
    positive_labels = np.zeros((len(positive)), dtype=np.int)
    negative_labels = np.ones((len(negative)), dtype=np.int)
    X = np.append(positive, negative, axis=0)
    y = np.append(positive_labels, negative_labels, axis=0)
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.3,
                                                        random_state=42)
    train_map_size = X_train.nbytes * 10
    test_map_size = X_test.nbytes * 10
    # Path where the train_lmdb is going to be created.
    # Be sure to erase the directory first if it is already created
    train_path = '/Users/Daniel/Documents/workspace/cnn-sentiment/words_model/train_lmdb'
    # Path where the test_lmdb is going to be created.
    test_path = '/Users/Daniel/Documents/workspace/cnn-sentiment/words_model/test_lmdb'
    train_env = lmdb.open(train_path,
                          map_size=train_map_size,
                          map_async=True,
                          max_dbs=0)
    with train_env.begin(write=True) as txn:
        for i in xrange(len(X_train)):
            datum = caffe.proto.caffe_pb2.Datum()
            datum.channels = X_train.shape[1]
            datum.height = X_train.shape[2]
            datum.width = X_train.shape[3]
            # datum.data if the data is integer, datum.float_data otherwise
            datum.data = X_train[i, :, :, :].tobytes()
            datum.label = y_train[i]
            str_id = '{:08}'.format(i)
            txn.put(str_id, datum.SerializeToString())
    test_env = lmdb.open(test_path,
                         map_size=test_map_size,
                         map_async=True,
                         max_dbs=0)
    with test_env.begin(write=True) as txn:
        for i in xrange(len(X_test)):
            datum = caffe.proto.caffe_pb2.Datum()
            datum.channels = X_train.shape[1]
            datum.height = X_test.shape[2]
            datum.width = X_test.shape[3]
            # datum.data if the data is integer, datum.float_data otherwise
            datum.data = X_test[i, :, :].tobytes()
            datum.label = y_test[i]
            str_id = '{:08}'.format(i)
            txn.put(str_id, datum.SerializeToString())
