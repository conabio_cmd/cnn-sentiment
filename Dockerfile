# Start with CUDA base image
FROM ezentenoj/caffe
MAINTAINER Daniel Zenteno <daniel.zenteno@conabio.gob.mx>

#Install jupyter
RUN pip install -U jupyter
RUN pip install -U joblib
RUN jupyter notebook --generate-config
COPY ./jupyter_notebook_config.py /root/.jupyter/jupyter_notebook_config.py 
RUN pip install scikit-learn
RUN pip install lmdb