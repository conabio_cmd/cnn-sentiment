# Named entity recognition using Convnets #

It can solve any two classes problem.


### Training ###

To train, please first edit the file **scientific_names/dataset/create.py**. Change the directories and the matrix shapes if needed. Generate the positive.pkl and negative.pkl.

To generate the lmdb files needed to train Caffe, run **scientific_names/dataset/create_from_freatures.py**. Change the directories.

Run Caffe using the files **words_model/quick_solver_features.proto** and **words_model/train_val_features.proto**. Change the directores of your lmdb files.

### Docker ###

This repository includes a docker-compose file and a Dockerfile with all the packages needed to run it out of the box. Change the docker-compose to have as **entrypoint** the scripts for training.
